#include "QueryProcessing.h"
#include "QueryOptimizer.h"
#include "CPUGPUProcessing.h"
#include "CacheManager.h"
#include "CPUProcessing.h"
#include "CostModel.h"

#include <cstdlib>

int main(int argc, char* argv[]) {
	if (argc < 2) {
		std::cerr << "Usage: ./program <query>" << std::endl;
		return 1;
	}

	std::string query = argv[1];
	cout << "Query: " << query << endl;
	cudaSetDevice(0);
	CUdevice device;
	cuDeviceGet(&device, 0);

	bool verbose = 0;

	srand(123);

	size_t size = 200 * 1024 * 1024; //200 MB
	size_t processing = 400 * 1024 * 1024; //400MB
	size_t pinned = 400 * 1024 * 1024; //400MB
	double alpha = 1.0;
	bool custom = false; // set to false from true
	bool skipping = true;

	cout << "Allocating " << size * 4 / 1024 / 1024 <<" MB GPU Cache and " << processing * 8 / 1024 / 1024 << " MB GPU Processing Region" << endl;
	
	CPUGPUProcessing* cgp = new CPUGPUProcessing(size, processing, pinned, verbose, custom, skipping);
	QueryProcessing* qp;

	cout << endl;

	bool exit = 0;
	string input, many, policy;
	int many_query;
	ReplacementPolicy repl_policy;
	double time = 0;
	double malloc_time_total = 0, execution_time = 0, optimization_time = 0, merging_time = 0;
	double time1 = 0, time2 = 0;
	unsigned long long cpu_to_gpu = 0, gpu_to_cpu = 0;
	unsigned long long cpu_to_gpu1 = 0, gpu_to_cpu1 = 0;
	unsigned long long cpu_to_gpu2 = 0, gpu_to_cpu2 = 0;
	unsigned long long repl_traffic = 0;
	double malloc_time_total1 = 0, execution_time1 = 0, optimization_time1 = 0, merging_time1 = 0;
	double malloc_time_total2 = 0, execution_time2 = 0, optimization_time2 = 0, merging_time2 = 0;
	Distribution dist = None;

	double mean = 1;

	qp = new QueryProcessing(cgp, verbose, dist);
    time = 0; malloc_time_total = 0; cpu_to_gpu = 0; gpu_to_cpu = 0; execution_time = 0; optimization_time = 0; merging_time = 0;
			cgp->resetTime();

			qp->setQuery(stoi(query));

			time1 = qp->processQuery();
			malloc_time_total1 = cgp->malloc_time_total;
			cpu_to_gpu1 = cgp->cpu_to_gpu_total;
			gpu_to_cpu1 = cgp->gpu_to_cpu_total;
			execution_time1 = cgp->execution_total;
			optimization_time1 = cgp->optimization_total;
			merging_time1 = cgp->merging_total;
			cgp->resetTime();

			time2 = qp->processQuery2();
			malloc_time_total2 = cgp->malloc_time_total;
			cpu_to_gpu2 = cgp->cpu_to_gpu_total;
			gpu_to_cpu2 = cgp->gpu_to_cpu_total;
			execution_time2 = cgp->execution_total;
			optimization_time2 = cgp->optimization_total;
			merging_time2 = cgp->merging_total;
			cgp->resetTime();

			if (time1 <= time2) {
				time += time1; cpu_to_gpu += cpu_to_gpu1; gpu_to_cpu += gpu_to_cpu1; malloc_time_total += malloc_time_total1;
				execution_time += execution_time1; optimization_time += optimization_time1; merging_time += merging_time1;
			} else {
				time += time2; cpu_to_gpu += cpu_to_gpu2; gpu_to_cpu += gpu_to_cpu2; malloc_time_total += malloc_time_total2;
				execution_time += execution_time2; optimization_time += optimization_time2; merging_time += merging_time2;
			}
				cout << endl;
		cout << "Cumulated Time: " << time << endl;
		cout << "CPU to GPU traffic: " << cpu_to_gpu  << endl;
		cout << "GPU to CPU traffic: " << gpu_to_cpu  << endl;
		cout << "Malloc time: " << malloc_time_total << endl;
		cout << "Execution time: " << execution_time << endl;
		cout << "Merging time: " << merging_time << endl;
		cout << endl;
}